import subprocess
import time
import typing


import pandas


def esearch_genus(genus: str) -> subprocess.Popen:
    return subprocess.Popen(
        [
            "esearch",
            "-db",
            "taxonomy",
            "-query",
            genus,
        ],
        stdout=subprocess.PIPE,
    )

def efetch(esearch_popen: subprocess.Popen) -> subprocess.Popen:
    return subprocess.Popen(
        [
            "efetch",
            "-db", "taxonomy",
            "-format", "xml",
        ],
        stdin=esearch_popen.stdout,
        stdout=subprocess.PIPE,
    )


def xtract_taxonomy(efetch_popen: subprocess.Popen) -> subprocess.Popen:
    return subprocess.Popen(
        [
            'xtract',
            '-pattern', 'Taxon',
            '-tab', ',',
            '-first', 'TaxId', 'ScientificName',
            '-group', 'Taxon', '-KING', '(-)', '-PHYL', '(-)', '-CLSS', '(-)', '-ORDR', '(-)', '-FMLY', '(-)', '-GNUS', '(-)',
            '-block', '*/Taxon', '-match', 'Rank:kingdom', '-KING',         'ScientificName',
            '-block', '*/Taxon', '-match', 'Rank:phylum', '-PHYL',         'ScientificName',
            '-block', '*/Taxon', '-match', 'Rank:class', '-CLSS',         'ScientificName',
            '-block', '*/Taxon', '-match', 'Rank:order', '-ORDR', 'ScientificName',
            '-block', '*/Taxon', '-match', 'Rank:family', '-FMLY',         'ScientificName',
            '-block', '*/Taxon', '-match', 'Rank:genus', '-GNUS', 'ScientificName',
            '-group', 'Taxon', '-tab', ',', '-element', '&KING', '&PHYL', '&CLSS', '&ORDR', '&FMLY', '&GNUS',
        ],
        stdin=efetch_popen.stdout,
        stdout=subprocess.PIPE,
    )

egg_database = pandas.read_csv(
    'egg_database.tsv',
    sep='\t',
)
genuses_set = sorted(set(egg_database.g))

taxonomy_file = open('taxonomy_file', 'w')

for genus in genuses_set:
    xtract_popen = xtract_taxonomy(efetch(esearch_genus(genus)))

    for line in xtract_popen.stdout.readlines():
        formatted_line = line.decode(encoding='utf-8').strip()
        taxonomy_file.write(f"egg_database.g: {genus}; entrez: {formatted_line};\n")

    taxonomy_file.flush()

    time.sleep(.5)
